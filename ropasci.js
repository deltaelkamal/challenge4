function getComputerChoice() {
    const comp = Math.random();
    if( comp < 0.34 ) return 'rock';
    if( comp >= 0.34 && comp < 0.67 ) return 'paper';
    return 'scissors';
}

function getResult(comp, player){
    if( player == comp ) return 'DRAW!';
    if( player == 'rock' ) return ( comp == 'paper' ) ? 'LOSE' : 'WIN';
    if( player == 'paper' ) return ( comp == 'scissors' ) ? 'LOSE' : 'WIN';
    if( player == 'scissors' ) return ( comp == 'rock' ) ? 'LOSE' : 'WIN';
}

const choice = document.querySelectorAll('li img');
choice.forEach(function(cho) {
    cho.addEventListener('click', function() {
        const computerChoice = getComputerChoice();
        const playerChoice = cho.className;
        const result = getResult(computerChoice, playerChoice);
        
        const imgComputer = document.querySelector('.img-computer');
        imgComputer.setAttribute('src', 'assets/' + computerChoice + '.png');
        
        const info = document.querySelector('.info');
        info.innerHTML = result;
    });
});
